#ifndef battery_H
#define	battery_H

#include "type.h"

void battery_init(void);
void battery_addValues(u8 v);
u8 battery_getAverage(void);
void battery_getValue(void);
void battery_main(void);

#ifdef test
void test_battery_getAverage(void);
#endif	/* test */

#endif	/* battery_H */