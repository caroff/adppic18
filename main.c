#include "def.h"
#include "init.h"
#include "battery.h"
#include "telecommande.h"
#include "deplacement.h"
#include "servo.h"
#include "sonar.h"

#include "struct_test.h"

// Pragma
#pragma config OSC = INTIO67
#pragma config PBADEN = OFF, WDT = OFF, LVP = OFF, DEBUG = ON

RobotState robot;

void main(void) {

    init();

    /*** Test mode ***/
#ifdef test
    struct_test();
    test_battery_getAverage();
    while(On);
#endif	/* test */

    /*** Normal mode ***/
    while (On) {
        LED_TEST_PIN = 1; // flag.robotON;
        telecommande_main();
        if (!robot.on) {
            deplacement_stop();
            continue;
        }
        battery_main();
        deplacement_main();
        servo_main();
        sonar_main();
    }
}
