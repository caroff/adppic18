#include "def.h"

#include "struct_test.h"

#ifdef test
void struct_test(void) {
    typedef union {
        struct {
            u8 led;
        };
        struct {
            bit led0 : 1;
            bit led1 : 1;
            bit led26 : 4;
            bit led6 : 1;
            bit led7 : 1;
        };
    } LED;

    LED x;
    x.led = 0;
    printf("x (0x00): 0x%02hX\r\n", x.led);
    x.led = 0x80;
    printf("x (0x80): 0x%02hX\r\n", x.led);
    x.led = 0xff;
    printf("x (0xFF): 0x%02hX\r\n", x.led);
    printf("---\r\n");
    x.led = 0;
    printf("x (0x00): 0x%02hX\r\n", x.led);
    x.led = 0;
    x.led0 = 1;
    printf("x (0x01): 0x%02hX\r\n", x.led);
    x.led = 0;
    x.led7 = 1;
    printf("x (0x80): 0x%02hX\r\n", x.led);
    x.led = 0;
    x.led26 = 1;
    printf("x (0x04): 0x%02hX\r\n", x.led);
    x.led = 0;
    x.led26 = 0b1001;
    printf("x (0x24): 0x%02hX\r\n", x.led);
    x.led = 0;
    x.led26 = 0b10001;
    printf("x (0x04): 0x%02hX\r\n", x.led);
    x.led = 0;
    x.led26 = 0b11111111;
    printf("x (0x3C): 0x%02hX\r\n", x.led);
    x.led = x.led26;
    printf("x (0xFF): 0x%02hX\r\n", x.led);
}
#endif
/* test */