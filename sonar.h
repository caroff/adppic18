#ifndef SONAR_H
#define	SONAR_H

u16 sonar_read(void);
void sonar_start(void);

void sonar_main(void);

void sonar_scanning_start(void);
void sonar_scanning_main(void);

void sonar_precise_scanning_start(void);
void sonar_precise_scanning_main(void);

#endif	/* SONAR_H */
