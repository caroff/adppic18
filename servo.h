#ifndef SERVO_H
#define	SERVO_H

#include "type.h"

void servo_main(void);
void servo_set(i8 angle); // angle between -90 and 90

#endif	/* SERVO_H */
