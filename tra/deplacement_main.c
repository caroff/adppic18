#include "def.h"
#include "deplacement.h"
#include "led.h"
#include "can.h"
#include "MI2C.h"

void deplacement_main() {
    can_getDistance();
    if (!robot.deplacement || robot.can.running != CanTarget_off)
        return;

    if (!robot.can.capteurGauche) {
        can_start(CAN_CAPTEUR_GAUCHE);
        return;
    }

    if (!robot.can.capteurDroit && !robot.can.running != CanTarget_off) {
        can_start(CAN_CAPTEUR_DROIT);
        return;
    }

    if (robot.can.capteurDroit && robot.can.capteurGauche) {
        distanceMoyenne = (distanceGauche + distanceDroite) / 2.f;
        led_from_distance(distanceMoyenne);

        printf(
            "distance Gauche: %d\r\n"
            "distance Droite: %d\r\n"
            "distance Moyenne: %d\r\n",
            distanceGauche, distanceDroite, distanceMoyenne
       );

        // ---------------- GESTION DES DISTANCES ------------------ \\

        autoAjust = T;
        if (!robot.objetTrouve) {
            if (distanceMoyenne > 100) // distanceMoyenne > 100cm
                vitesse = VITESSE_MAX;
            else if (distanceMoyenne < 35) { // distanceMoyenne < 35cm
                robot.objetTrouve = T;
                autoAjust = F;
            } else if (distanceMoyenne < 45) { // distanceMoyenne < 55cm
                vitesse = VITESSE_MAX * 0.35;
                autoAjust = F;
            } else if (distanceMoyenne < 55) {// distanceMoyenne < 55cm
                vitesse = VITESSE_MAX * 0.5;
            } else if (distanceMoyenne < 70) // distanceMoyenne < 70cm
                vitesse = VITESSE_MAX * 0.75;

        } else {
            differenceDroiteGauche = 0;
            vitesse = 0;
        }

        // ------------------ GESTION DES VITESSES ------------------ \\

        if (autoAjust)
            differenceDroiteGauche += ((int) TMR0L - (int) TMR1L);
        else
            differenceDroiteGauche = 0;
#ifdef debug
        printf("difference gauche: %d  -- TMROL %d  TMR1L %d\r\n", differenceDroiteGauche, TMR0L, TMR1L);
#endif
        TMR0L = 0;
        TMR1L = 0;
        //differenceDroiteGauche = 0;

        if (differenceDroiteGauche == 0) { // Parfait le robot va tout droit
            vitesseDroite = vitesse;
            vitesseGauche = vitesse;
        } else if (differenceDroiteGauche > 0) { // Le robot va l�g�rement sur le cot� droit
            vitesseGauche = vitesse;
            vitesseDroite = (vitesse - differenceDroiteGauche) > 0 ? (vitesse - differenceDroiteGauche) : 0;
        } else { // Le robot va l�g�rement sur le cot� gauche
            vitesseGauche = (vitesse + differenceDroiteGauche) > 0 ? (vitesse + differenceDroiteGauche) : 0;
            vitesseDroite = vitesse;
        }

#ifdef debug
        printf("vitesse Gauche: %d\r\nvitesse Droite: %d\r\nvitesse Moyenne: %d\r\n", vitesseGauche, vitesseDroite, vitesse);
#endif

        CCPR1L = (vitesseGauche > VITESSE_MAX) ? VITESSE_MAX : (vitesseGauche < 0 ? 0 : vitesseGauche);
        CCPR2L = (vitesseDroite > VITESSE_MAX) ? VITESSE_MAX : (vitesseDroite < 0 ? 0 : vitesseDroite);


        // ------------------------ FIN ------------------------ \\

        capteurDroitUpdate = F;
        capteurGaucheUpdate = F;
        robot.can.capteurDroit = F;
        robot.can.capteurGauche = F;
        robot.deplacement = F;
    }
}
