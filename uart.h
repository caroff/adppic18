#ifndef UART_H
#define	UART_H

#ifdef debug
#include <stdio.h>
#else
void printf(...);
#endif

void init_uart(void);

#endif	/* UART_H */
