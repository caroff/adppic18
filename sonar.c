#include "def.h"
#include "sonar.h"
#include "servo.h"
#include "MI2C.h"

#define ANGLE_OFFSET 12;

u16 sonar_read(void) {
//    Location | Read | Write
//    0 | Software Revision | Command Register
//    1 | Unused (reads 0x80) | N/A
//    2 | Range High Byte | N/A
//    3 | Range Low Byte | N/A
//    4 | Autotune Minimum - High Byte | N/A
//    5 | Autotune Minimum - Low Byte | N/A
// The ranging lasts up to 65mS, and the SRF02 will not respond to commands on
// the I2C bus whilst it is ranging.
    char location = 2;
    return i2c_sonar_read(SONAR_I2C_ADDRESS, location);
}

void sonar_start(void) {
    u8 command = 0;
    i2c_sonar_write(SONAR_I2C_ADDRESS, command);
}

void sonar_main(void) {
    if (robot.sonar.scanning) {
        sonar_scanning_main();
    } else if (robot.sonar.precise_scanning) {
        sonar_precise_scanning_main();
    }
}

void sonar_scanning_start(void) {
    servo_set(-90);
    robot.sonar.best_angle = 127;
    robot.sonar.best_distance = ~0;
    robot.sonar.scanning = On;
}

void sonar_scanning_main(void) {
    u16 read;
    if (robot.sonar.rotating) {
        robot.sonar.rotating = On;
        sonar_start();
        read = sonar_read();

        if (read < robot.sonar.best_distance) {
            robot.sonar.best_angle = robot.servo.angle - ANGLE_OFFSET;
            robot.sonar.best_distance = read;
        }
        if (robot.servo.angle >= 90) {
            robot.sonar.scanning = Off;
        }
    } else {
        servo_set(robot.servo.angle + 20);
        robot.sonar.rotating = On;
    }
}

void sonar_precise_scanning_start(void) {
    servo_set(-20);
    robot.sonar.best_angle = 127;
    robot.sonar.best_distance = ~0;
    robot.sonar.precise_scanning = On;
}

void sonar_precise_scanning_main(void) {
    u16 read;
    servo_set(robot.servo.angle + 5);
    sonar_start();
    read = sonar_read();

    if (read < robot.sonar.best_distance) {
        robot.sonar.best_angle = robot.servo.angle;
        robot.sonar.best_distance = read;
    }
    if (robot.servo.angle >= 90) {
        robot.sonar.precise_scanning = Off;
    }
}
