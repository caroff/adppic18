
#include "def.h"
#include "servo.h"

void servo_main(void) {
}

void servo_set(i8 angle) {
    if (angle < -90) {
        printf("servo_set: angle < -90, out of range");
    } else if (angle > 90) {
        printf("servo_set: angle > +90, out of range");
    } else {
        robot.servo.angle = angle;
        robot.servo.threshold = 270 + (i16) angle;
    }
}