#include "def.h"

#include "init.h"
#include "MI2C.h"
#include "battery.h"
#include "uart.h"
#include "pic18f2520/p18f2520.h"

//battery
extern u8 ubat; // D�claration dans le main.c

void init() {

    //Oscillator 8MHz
    OSCCONbits.IRCF = 0b111; // Internal Oscillator Frequency Select bits
    // 111 -> 8MHz
    OSCCONbits.SCS = 0b10; // Use Internal Oscillator

    init_uart();

    printf("/\\Debut init\r\n");

    /*--- Pin IO configuration ---*/
    // Configuration des pins en entr�es ou en sorties
    /* Pin en entr�e (= 1) */
    // CAN
    TRISAbits.RA0 = 1; // Capteurs Infrarouge Droit
    TRISAbits.RA1 = 1; // Capteurs Infrarouge Gauche
    TRISAbits.RA2 = 1; // Acquisition tension battery

    TRISBbits.RB0 = 1; // Interruption de RESET

    /* Pin en sortie (= 0) */
    TRISBbits.RB1 = 0; // Alimentation des capteurs infrarouge
    TRISBbits.RB5 = 0; // Led de test
    // Moteurs
    TRISAbits.RA6 = 0; // Direction droite
    TRISAbits.RA7 = 0; // Direction gauche
    TRISCbits.RC1 = 0; // PWM moteur droite
    TRISCbits.RC2 = 0; // PWM moteur gauche
    TRISCbits.RC5 = 0; // PWM sonar

    /*--- Niveau de tension � l'initialisation ---*/
    PORTBbits.RB1 = 0; // Infrarouge OFF
    PORTAbits.RA6 = 1; // Direction droite -> FORWARD
    PORTAbits.RA7 = 1; // Direction gauche -> FORWARD

    // Note: Les pin du I2C est initialis�e dans


    // Interruption
    INTCON2bits.INTEDG0 = 0;
    INTCONbits.INT0IE = 1;
    INTCONbits.PEIE = 1;
    INTCONbits.GIE = 1;
    PIE1bits.ADIE = 1;
    PIE1bits.TMR2IE = 1;
    PIR1bits.ADIF = 0;

    // PWM
    T2CONbits.T2CKPS = 0b10; //Precal x16
    T2CONbits.T2OUTPS = 0b1001; //Post x10
    PR2 = 123; // 19 680
    CCP1CONbits.DC1B = 0b00; //CCP1 Moteur Gauche
    CCP2CONbits.DC2B = 0b00; //CCP2 Moteur Droit
    CCP1CONbits.CCP1M = 0b1100; //mode PWM
    CCP2CONbits.CCP2M = 0b1100; //mode PWM
    CCPR1L = 0;
    CCPR2L = 0;
    T2CONbits.TMR2ON = 1;

    // Timer 0
    TRISAbits.RA4 = 1;
    T0CONbits.TMR0ON = 1; // Activer timer0
    T0CONbits.T08BIT = 0; // Timer0 16 bit
    T0CONbits.T0CS = 1; // PA
    T0CONbits.T0SE = 0; // Front montant
    TMR0L = 0;
    TMR0H = 0;

    // Timer 1
    TRISCbits.RC0 = 1;
    T1CONbits.TMR1ON = 1; // Activer timer1
    T1CONbits.RD16 = 1; // Op�ration 16 bits
    T1CONbits.T1RUN = 0; // PB
    T1CONbits.T1OSCEN = 0; // Oscillator d�sactiver
    T1CONbits.T1SYNC = 1; // Pas synchronis�
    T1CONbits.TMR1CS = 1; // PB
    TMR1L = 0;
    TMR1H = 0;

    /*--- Capteur Infrarouge ---*/
    ADCON0bits.ADON = 1;
    ADCON1bits.PCFG = 0b1100;
    ADCON1bits.VCFG = 0b00;
    ADCON2bits.ADCS = 0b100; // 1us horloge de conversion
    ADCON2bits.ACQT = 0b010;
    ADCON2bits.ADFM = 0;


    //battery
    battery_init();

    // Variables
    robot_state_init();

    //I2C
    i2c_init();

    printf("\\/Fin init\r\n");
}

void robot_state_init() {
    //Timer
    robot.time = 0;

    //Flags
    robot.battery_ = On; // Vrai toutes les 2 secondes
    robot.telecommande = Off; // Vrai toutes les 50 ms
    robot.lireTelecommande_ = On; // Vrai toutes les 500ms
    robot.deplacement_ = On; // Vrai toutes les 100 ms
    robot.leds_ = On;
    robot.on = Off; // Vrai le robot marche
    robot.objetTrouve_ = Off; // l'objet est initalement pas trouv�

    //Flags Conertisseur Analogique Num�rique
    robot.can.battery = Off; // Vrai si la conversion AN est finit
    robot.can.capteurGauche = Off; // Vrai si la conversion AN est finit
    robot.can.capteurDroit = Off; // Vrai si la conversion AN est finit
    robot.can.running = CanTarget_off; // Aucune conversion en cours
}

