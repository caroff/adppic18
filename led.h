#ifndef LED_H
#define	LED_H

#include "type.h"

#define DIST_1_LED 35
#define DIST_2_LED 45
#define DIST_3_LED 60
#define DIST_4_LED 75
#define DIST_5_LED 90
#define DIST_6_LED 105
#define DIST_7_LED 120
#define DIST_8_LED 140


void led_from_distance(i16 distanceMoyenne);

#endif	/* LED_H */

