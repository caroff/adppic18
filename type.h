#ifndef TYPE_H
#define	TYPE_H

/*------------------------------ #typdef ---------------------------*/

typedef unsigned bit;

typedef unsigned char bool;
typedef unsigned char u8;
typedef   signed char i8;

typedef unsigned short u16;
typedef   signed short i16;

typedef unsigned short long u24;
typedef   signed short long i24;

typedef unsigned long u32;
typedef   signed long i32;

#endif	/* TYPE_H */

