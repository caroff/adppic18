#include "def.h"
#include "battery.h"

u8 measure_a[BATTERY_SAMPLE_COUNT];
i16 battery_measure_index = 0;

void battery_init() {
    i16 i = 0;
    for (i = 0; i < BATTERY_SAMPLE_COUNT; i++) {
        measure_a[i] = LIMIT_BATTERY;
    }
}

void battery_main() {
    battery_getValue();
    if (!robot.battery_ || robot.can.running != CanTarget_off) {
        return;
    }
    robot.battery_ = Off;
    robot.can.running = CanTarget_battery;
    ADCON0bits.CHS = 0b0010;
    ADCON0bits.GO = 1;
}

void battery_getValue() {
    if (!robot.can.battery) {
        return;
    }
    battery_addValues(ADRESH);
    robot.can.running = CanTarget_off;
    robot.can.battery = Off;
    printf("battery : %d\r\n", battery_getAverage());
    if (battery_getAverage() < LIMIT_BATTERY) {
        printf("low battery - switching robot off");
        robot.on = Off;
    }
    
}

void battery_addValues(u8 v) {
    measure_a[battery_measure_index] = v;
    battery_measure_index = (1 + battery_measure_index) % BATTERY_SAMPLE_COUNT;
}

u8 battery_getAverage() {
    i16 i = 0;
    i16 moyenne = 0;
    for (i = 0; i < BATTERY_SAMPLE_COUNT; i++) {
        moyenne += measure_a[i];
    }
    return moyenne / BATTERY_SAMPLE_COUNT;
}

void test_battery_getAverage() {
    u8 count = 4;
    if (BATTERY_SAMPLE_COUNT != count) {
        printf("MAUVAIS NOMBRE D'ECHANTILLONS BATTERIE : %d\r\n", BATTERY_SAMPLE_COUNT);
    } else {
        battery_addValues(200);
        battery_addValues(100);
        battery_addValues(100);
        battery_addValues(80);
//        printf("test_battery_getAverage: %d %d %d %d\r\n",
//            measure_a[0], measure_a[1], measure_a[2], measure_a[3]);
        printf("test_battery_getAverage (120): %d \r\n", battery_getAverage());
    }
    return;
}

void nothing(void) {
}