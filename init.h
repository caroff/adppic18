#ifndef INIT_H
#define	INIT_H

#include "type.h"

void init(void);
void initTimer(void);
void robot_state_init(void);
void initI2C(void);

#endif	/* INIT_H */
