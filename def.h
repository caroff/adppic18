#ifndef DEF_H
#define DEF_H

/*------------------------------ Define ---------------------------*/
// Booleen
typedef enum
{
    Off = 0,
    On = 1
};

// Mode debug
#define debug
// #define test

/*------------------------------ Include ---------------------------*/
#include <p18f2520.h>
#include "type.h"
#include "uart.h"

/*------------------------------ Define ---------------------------*/

// Ports
#define LED_TEST_PIN PORTBbits.RB5
#define DEPLACEMENT_LDIRECTION PORTAbits.RA7
#define DEPLACEMENT_RDIRECTION PORTAbits.RA6

// battery
#define LIMIT_BATTERY 159
#define BATTERY_SAMPLE_COUNT 4

// I2C
// Leds PC PCF8574
#define LED_I2C_ADDRESS 0x40
// Sonar
#define SONAR_I2C_ADDRESS 0xE0
// Telecommande
#define TELECOMMANDE_I2C_ADDRESS 0xA2

// Telecommande
typedef enum
{
    TelecommandeInput_right = 0x31,
    TelecommandeInput_forward = 0x32,
    TelecommandeInput_on_off = 0x33,
    TelecommandeInput_back = 0x34,
    TelecommandeInput_left = 0x35
} TelecommandeInput;


// P�riode flags
#define PERIODE_BATTERY 200               // 2    secondes (200 * 0.010 = 2)
#define PERIODE_TELECOMMANDE 500          // 500 msecondes (500 * 0.010 = 0.50)
#define PERIODE_LED 25                    // 250 msecondes  (25 * 0.010 = 0.25)
#define PERIODE_DEPLACEMENT 10            // 100 msecondes  (10 * 0.010 = 0.1)

// Convertisseur Analogique num�rique
typedef enum
{
    CanTarget_rSensor = 0,   // AN0
    CanTarget_lSensor = 1,   // AN1
    CanTarget_battery = 2,   // AN2
    CanTarget_off = 3
} CanTarget;
#define CAN_DONE PIR1bits.ADIF

// Servomoteur
#define SERVO_PWM_PIN_OUT TRISCbits.RC5

/*--------------------------- typedef struct ------------------------*/

// Move instruction
typedef struct
{
    u8 speed; // 0 is stop; 255 is max speed
    u8 duration; // 0 is 0 ms; 255 is 2550 ms
    i8 rotationSpeed; // 127 is to the left; -128 is to the right
} DeplacementInstruction;


// State
typedef struct
{
    u8 duration;
    DeplacementInstruction* instruction_array;
    u8 segment_index;
    u8 segment_count;

    u8 lSpeed; // 0 is stop; 255 is max speed
    u8 rSpeed;
    i8 lDirection; // 1 is forward, -1 backward, 0 is stop
    i8 rDirection;
} DeplacementState;

typedef struct
{
    u8 led;
} LedState;

typedef struct
{
    bit running : 2;
    bit battery : 1;
    bit capteurGauche : 1;
    bit capteurDroit : 1;
    bit : 3;
} CanState;

typedef struct
{
    i8 angle;
    u16 threshold;
} ServoState;

typedef struct
{
    i8 best_angle;
    u16 best_distance;
    bit running : 1;
    bit scanning : 1;
    bit precise_scanning : 1;
    bit rotating : 1;
    bit : 4;
} SonarState;

typedef struct
{
    i16 time;
    DeplacementState deplacement;
    LedState led;
    ServoState servo;
    SonarState sonar;
    CanState can;
    bit on : 1;
    bit battery_ : 1;
    bit telecommande : 1;
    bit lireTelecommande_ : 1;
    bit leds_ : 1;
    bit deplacement_ : 1;
    bit objetTrouve_ : 1;
} RobotState;

/*------------------------------ extern ---------------------------*/

extern RobotState robot;

#endif /* DEF_H */
