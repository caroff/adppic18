#include "def.h"

void HighISR(void);

#pragma code HighVector=0x08
void IntHighVector(void) {
    _asm goto HighISR _endasm
}
#pragma code

#pragma interrupt HighISR
void HighISR(void) {
    if (CAN_DONE) { // CAN
        CAN_DONE = 0;
        switch (robot.can.running) {
            case CanTarget_off:
            {
                // Erreur;
                printf("Unexpected CAN_SLEEPING after ADIF\r\n");
                break;
            }
            case CanTarget_battery:
            {
                robot.can.battery = On;
                break;
            }
            case CanTarget_lSensor:
            {
                robot.can.capteurGauche = On;
                break;
            }
            case CanTarget_rSensor:
            {
                robot.can.capteurDroit = On;
                break;
            }
        }
    }
    if (PIR1bits.TMR2IF) { // Timer - 10ms
        PIR1bits.TMR2IF = 0;
        robot.time++;
        robot.time %= 3600; // Servo
        SERVO_PWM_PIN_OUT = robot.time < robot.servo.threshold;

        robot.battery_ |= robot.time % PERIODE_BATTERY == 0;
        // Vrai toutes les 2 secondes

        robot.deplacement_ |= robot.time % PERIODE_DEPLACEMENT == 0;
        // Vrai toutes les 10 msecondes
        
        robot.leds_ |= robot.time % 50 == 0;
        // Vrai toutes les 500 msecondes
        
        robot.lireTelecommande_ |= robot.time % PERIODE_TELECOMMANDE == 0;
        // Vrai toutes les 500 msecondes
    }
    if (INTCONbits.INT0IF/* && flag.lireTelecommande */) { // Telecommande
        robot.lireTelecommande_ = Off;
        robot.telecommande = On;
        INTCONbits.INT0IF = 0;
    }
}
