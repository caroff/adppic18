#ifndef CAN_H
#define	CAN_H

#include "type.h"

#define CAN_CAPTEUR_DROIT 0b0000   // AN0
#define CAN_CAPTEUR_GAUCHE 0b0001  // AN1
#define CAN_battery 0b0010        // AN2


#define CAN_CHANNEL ADCON0bits.CHS
#define CAN_RUNNING ADCON0bits.GO
#define CAN_GO      ADCON0bits.GO


#define CAN_NBR_DIST_ETALONNE 14

void can_start(u8 capteur);
void can_getDistance(void);
u16 can_getValueCentimeter(u8 v);

#endif	/* CAN_H */
