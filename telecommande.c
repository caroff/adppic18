#include "def.h"
#include "telecommande.h"
#include "MI2C.h"

u8 buffer[3];

void telecommande_main() {
    if (robot.telecommande == Off)
        return;

    robot.telecommande = Off;
    i2c_telecomand_read(TELECOMMANDE_I2C_ADDRESS, (char*) buffer);

    if (!robot.on) {
        if (buffer[1] == TelecommandeInput_on_off) {
            robot.on = On;
            printf("TOUCHE CENTRE\r\n");
            printf("ROBOT ON\r\n");
            return;
        }
    }

    switch (buffer[1]) {
        case TelecommandeInput_on_off:
        {
            robot.on = !robot.on;
            printf("TOUCHE CENTRE\r\n");
            printf("ROBOT OFF\r\n");
            break;
        }
        case TelecommandeInput_right:
        {
            printf("TOUCHE DROITE\r\n");
            break;
        }
        case TelecommandeInput_left:
        {
            printf("TOUCHE GAUCHE\r\n");
            break;
        }
        case TelecommandeInput_forward:
        {
            printf("TOUCHE AVANT\r\n");
            break;
        }
        case TelecommandeInput_back:
        {
            printf("TOUCHE ARRIERE\r\n");
            break;
        }
    }
}
