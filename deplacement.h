#ifndef DEPLACEMENT_H
#define	DEPLACEMENT_H

#include "type.h"

#define deplacement_tick()\
(robot.deplacement_.duration += !robot.deplacement_.duration - 1)

#define deplacement_moving()\
(robot.deplacement.segment_index < robot.deplacement.segment_count)

void deplacement_main(void);
void deplacement_next_segment(void);
void deplacement_IO_update(void);
void deplacement_compute_motor_speed(DeplacementInstruction* in, DeplacementState* out);
void deplacement_require(DeplacementInstruction* instruction_array, u8 segment_count);

#ifdef debug
void deplacement_test(void);
#endif

void deplacement_stop(void);

#endif	/* DEPLACEMENT_H */

