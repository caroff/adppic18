#include "def.h"
#include "led.h"
#include "MI2C.h"

#define led_set(LED) robot.led.led = LED
#define led_update() i2c_led_write(LED_I2C_ADDRESS, ~robot.led.led)
#define led_send(LED) { led_set((LED)); led_update(); }

void led_from_time() {
    led_set(robot.time);
}

void led_from_distance(i16 distance_moyenne) {
    i16 d = distance_moyenne;
    if (d < DIST_4_LED) {
        if (d < DIST_2_LED) {
            if (d < DIST_1_LED) {
                led_send(0b00000000);
            } else {
                led_send(0b00000001);
            }
        } else {
            if (d < DIST_3_LED) {
                led_send(0b00000011);
            } else {
                led_send(0b00000111);
            }
        }
    } else {
        if (d < DIST_6_LED) {
            if (d < DIST_5_LED) {
                led_send(0b00001111);
            } else {
                led_send(0b00011111);
            }
        } else {
            if (d < DIST_7_LED) {
                led_send(0b00111111);
            } else {
                if (d < DIST_8_LED) {
                    led_send(0b01111111);
                } else {
                    led_send(0b11111111);
                }
            }
        }
    }
}