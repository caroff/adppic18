#include "type.h"
#include "def.h"

#ifndef debug
void printf(...) {}
#endif

#ifdef debug
void init_uart(void) {
    //Uart
    // Target Baud Rate = 9.6 KHz
    BAUDCONbits.BRG16 = 0; // 16-bit Baud Rate Register Enable bit = 8-bit: SPBRG only
    TXSTAbits.SYNC = 0; // Asynchronous mode (UART, not USART)
    TXSTAbits.BRGH = 0; // High Baud Rate Select bit (Asynchronous mode): Low speed
    TXSTAbits.TXEN = 1; // Transmit Enable bit
    // Baud Rate = F_OSC / [64 (n + 1)] avec n = SPBRG
    // n = F_OSC / (64 * Baud Rate) - 1
    // SPBRG = n = 12
    SPBRG = 12;
    RCSTAbits.SPEN = 1; // Serial Port Enable bit
    RCSTAbits.CREN = 1; // Continuous Receive Enable bit
}
#else
void init_uart(void) {}
#endif
