#include "def.h"
#include "deplacement.h"

#define VITESSE_MAX 50

void deplacement_main(void) {
    if (deplacement_moving() && !robot.deplacement.duration) {
        deplacement_next_segment();
        deplacement_IO_update();
    }
}

void deplacement_next_segment(void) {
    robot.deplacement.segment_index++;
    if (deplacement_moving()) {
        deplacement_compute_motor_speed(
            robot.deplacement.instruction_array + robot.deplacement.segment_index,
            &robot.deplacement
        );
    } else {
        robot.deplacement.segment_index = robot.deplacement.segment_count;
    }
}

void deplacement_IO_update(void) {
    DEPLACEMENT_LDIRECTION = robot.deplacement.lDirection;
    DEPLACEMENT_RDIRECTION = robot.deplacement.rDirection;
    CCPR1L = robot.deplacement.lSpeed;
    CCPR2L = robot.deplacement.rSpeed;
}

void deplacement_compute_motor_speed(DeplacementInstruction* in, DeplacementState* out) {
    u8 speed = in->speed;
    i8 rotation = in->rotationSpeed;
    i8 lspeed = (i8) (speed >> 1) + (rotation >> 2);
    i8 rspeed = (i8) (speed >> 1) - (rotation >> 2);
    i8 lDirection = (lspeed >= 0);
    i8 rDirection = (rspeed >= 0);
    out->duration = in->duration;
    out->lDirection = lDirection;
    out->rDirection = rDirection;
    out->lSpeed = (u8) (lDirection * lspeed);
    out->rSpeed = (u8) (rDirection * rspeed);
}

void deplacement_require(DeplacementInstruction* instruction_array, u8 segment_count) {
    robot.deplacement.instruction_array = instruction_array;
    robot.deplacement.segment_index = 0;
    robot.deplacement.segment_count = segment_count;
}

#ifdef debug
DeplacementInstruction deplacement_test_instruction[] = {
    { 128, 100,    0 },
    {   0, 100,  120 },
    {  64, 200,    0 },
    {   0, 100, -120 },
    { 250,  50,    0 }
};

const u8 deplacement_test_instruction_count = 5;

void deplacement_test() {
    deplacement_require(deplacement_test_instruction, deplacement_test_instruction_count);
}
#endif

void deplacement_stop() {
    CCPR1L = 0;
    CCPR2L = 0;
}
