#ifndef _MI2C_H_
#define _MI2C_H_


void i2c_init(void);
char i2c_detect(char adresse_i2c);


// Pour les transactions du SONAR
int i2c_sonar_read(char i2c_address, char distance_register);
void i2c_sonar_write(char i2c_address, char command);


// Pour la tÚlÚcommande
char i2c_telecomand_read(char adresse_i2c, char* Recv_Buff);


// Pour les transactions des PCF8574
char i2c_led_write(char adresse_i2c, char data );


#endif