#include "def.h"
#include "can.h"

bool capteurGaucheUpdate = Off;
bool capteurDroitUpdate = Off;

i16 distanceGauche = 0;
i16 distanceDroite = 0;

u8 distEtalonne[CAN_NBR_DIST_ETALONNE] = {
    128, 102, 77, 64, 56, 46, 41, 38, 36, 33, 31, 26, 23, 20
//   20   30  40  50  60  70  80  90 100 110 120 130 140 150 (distance en cm)
};

void can_start(u8 signal_target) {
//    switch (signal_target) {
//        case CAN_battery:
//            robot.can.running = CAN_RUNNING_battery;
//            ADCON0bits.CHS = CAN_RUNNING_battery;
//        break;
//        case CAN_CAPTEUR_DROIT:
//            robot.can.running = CAN_RUNNING_CAPTEUR_DROIT;
//            ADCON0bits.CHS = CAN_CAPTEUR_DROIT;
//        break;
//        case CAN_CAPTEUR_GAUCHE:
//            robot.can.running = CAN_RUNNING_CAPTEUR_GAUCHE;
//            ADCON0bits.CHS = CAN_CAPTEUR_GAUCHE;
//        break;
//    }
    robot.can.running = signal_target;
    CAN_CHANNEL = signal_target;
    CAN_GO = 1;
}

void can_getDistance() {
    if (robot.can.capteurDroit && !capteurDroitUpdate) {
        capteurDroitUpdate = On;
        distanceDroite = can_getValueCentimeter(ADRESH);
        robot.can.running = CanTarget_off;
    }
    if (robot.can.capteurGauche && !capteurGaucheUpdate) {
        capteurGaucheUpdate = On;
        distanceGauche = can_getValueCentimeter(ADRESH);
        robot.can.running = CanTarget_off;
    }
}

//u16 can_getValueCentimeter(u8 v) {
//    i16 i = CAN_NBR_DIST_ETALONNE - 1;
//    if (v <= distEtalonne[i])
//        return 150;
//    for (i = CAN_NBR_DIST_ETALONNE - 2; i >= 0; i--) {
//        if (v < distEtalonne[i]) {
//                float u = i + \
//                (distEtalonne[i] - v) \
//                / \
//                (float) ((distEtalonne[i] - distEtalonne[i + 1]));\
//            return (int) (20 + 10 * u);
//        }
//    }
//    return 20;
//}

u16 can_getValueCentimeter(u8 v) {
//    u16 inverse = 7630 / (10 * (u16) v + 418);
//    return inverse * inverse;
    // 7630 * 7630 -> 58216900
    u32 linear = 10 * (u16) v + 418;
    return (u16) (58216900L / (linear * linear));
}